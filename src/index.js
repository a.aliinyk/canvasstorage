console.log("heeello");
// import { Stage, Shape } from "@createjs/easeljs";

import imgLink from "./assets/test5.jpg";
console.log(imgLink);
const canvas = document.getElementById("c1");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const ctx = canvas.getContext("2d");

const img = new Image();
img.src = imgLink;
// img.width = window.innerWidth;
// img.height = window.innerHeight;

function Osc(speed) {
  var frame = 700;
  // current osc on iterration
  this.current = function(x) {
    frame > 0 ? (frame -= 0.009 * speed) : 0;

    return Math.sin(frame * speed);
  };
}

let osc = new Osc(0.03);
let osc2 = new Osc(0.09);
let osc3 = new Osc(0.03);

// osc function

img.onload = () => {
  let w = img.naturalWidth,
    h = img.naturalHeight,
    // starter points for img parts
    y0 = 0,
    y1 = h * 0.25,
    y2 = h * 0.5,
    y3 = h * 0.75,
    y4 = h;

  let wC = canvas.width,
    hC = canvas.height,
    // starter points for img parts
    yc0 = 0,
    yc1 = hC * 0.25,
    yc2 = hC * 0.5,
    yc3 = hC * 0.75,
    yc4 = hC;

  ctx.drawImage(img, 0, 0, window.innerWidth, window.innerHeight);

  setTimeout(() => {
    canvas.style.opacity = "1";
    canvas.style.filter = "blur(0px)";
    let count = 0;
    let anim = requestAnimationFrame(loop);
    let amplitude = 0;
    let aInc = 3;
    function loop() {
      count++;
      ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);

      if (amplitude === 135) aInc = -3;
      console.log(aInc);
      amplitude += aInc;

      for (let x = 0; x < wC + 3; x = x + 3) {
        let aCY1 = yc1 + osc.current(x * 0.2) * amplitude,
          aCY2 = yc2 + osc2.current(x * 0.2) * amplitude,
          aCY3 = yc3 + osc3.current(x * 0.2) * amplitude;

        let hc0 = aCY1,
          hc1 = aCY2 - aCY1,
          hc2 = aCY3 - aCY2,
          hc3 = yc4 - aCY3;

        ctx.drawImage(img, x * (w / wC), 0, 3 * (w / wC), y1, x, 0, 3, hc0);
        ctx.drawImage(
          img,
          x * (w / wC),
          y1,
          3 * (w / wC),
          y2 - y1,
          x,
          aCY1,
          3,
          hc1
        );
        ctx.drawImage(
          img,
          x * (w / wC),
          y2,
          3 * (w / wC),
          y3 - y2,
          x,
          aCY2,
          3,
          hc2
        );
        ctx.drawImage(
          img,
          x * (w / wC),
          y3,
          3 * (w / wC),
          y4 - y3,
          x,
          aCY3,
          3,
          hc3
        );
      }
      console.log(count);
      if (count < 90) anim = requestAnimationFrame(loop);
    }
  }, 400);
};
