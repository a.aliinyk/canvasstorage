// import { Stage, Shape } from "@createjs/easeljs";

let drawing = false;
const canvas = document.getElementById("c1");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
console.log(window.innerWidth, window.innerHeight);
const ctx = canvas.getContext("2d");

// ctx.fillStyle = "red";
// ctx.fillRect(0, 0, 100, 100);
// ctx.fillStyle = "blue";
// ctx.fillRect(50, 50, 100, 100);
// canvas.addEventListener("mouseup", e => {
//   drawing = false;
// });
// canvas.addEventListener("mouseout", e => {
//   drawing = false;
// });
// canvas.addEventListener("mousedown", e => {
//   drawing = true;
// });
// canvas.addEventListener("mousemove", e => {
//   let x = e.offsetX;
//   let y = e.offsetY;
//   if (drawing) {
//     ctx.fillRect(x, y, 20, 20);
//   }
// });

const dotsStorage = [];
for (let i = 0; i < 1000; i++) {
  dotsStorage.push({
    x: (Math.random() * 10 * Math.random() * i) % window.innerWidth,
    y: (Math.random() * 10 * Math.random() * i) % window.innerHeight
  });
}

function drawImage(cords) {
  ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
  dotsStorage.map(dot => {
    ctx.fillRect(dot.x, dot.y, 10, 10);
    // todo: we can base only on diff
    let differenceX = cords.x - dot.x;
    let differenceY = cords.y - dot.y;
    let lenght = Math.pow(
      Math.pow(differenceX, 2) + Math.pow(differenceY, 2),
      0.5
    );

    if (lenght < 200) {
      ctx.beginPath();
      ctx.moveTo(cords.x, cords.y);
      ctx.lineTo(dot.x, dot.y);
      ctx.stroke();
      ctx.closePath();
      dot.x = dot.x + differenceX / 100;
      dot.y = dot.y + differenceY / 100;
    }

    // dot.x = dot.x + Math.random() * 10;
  });
}
canvas.addEventListener("mousemove", e => {
  const cords = { x: e.offsetX, y: e.offsetY };
  drawImage(cords);
});
